<?php

/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 12.07.15
 * Time: 16:45
 */

namespace Calculator;

use Calculator\Exceptions\TypeException;
use Calculator\Token\Type\ATokenType;

class WordTest extends \PHPUnit_Framework_TestCase {

    public function testConstruct() {
        $Word = null;
        try {
            $Word = new Word(ATokenType::PLUS, '+');
        } catch (TypeException $TE) {
            $this->fail('Unknown type exception: '.$TE->getMessage());
        } catch (\Exception $E) {
            $this->fail('Unknown exception: '.$E->getMessage());
        }

        $this->assertTrue($Word instanceof Word);
    }

    public function testNConstruct() {
        $Word = null;
        try {
            $Word = new Word('Type', '+');
        } catch (TypeException $TE) {
            $this->assertTrue(true);
        } catch (\Exception $E) {
            $this->fail('Unknown exception: '.$E->getMessage());
        }

        $this->assertFalse($Word instanceof Word);
    }

    public function testRun() {
        $Word = new Word(ATokenType::PLUS, '+');
        $this->assertEquals(2, $Word->run(1, 1));
    }

    public function testVal() {
        $data = '+';
        $Word = new Word(ATokenType::PLUS, $data);
        $this->assertEquals($data, $Word->val());
    }
}
