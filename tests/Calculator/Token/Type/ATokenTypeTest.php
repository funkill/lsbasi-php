<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 12.07.15
 * Time: 14:55
 */

namespace Calculator\Token\Type;


class ATokenTypeTest extends \PHPUnit_Framework_TestCase {

    public function testCreate() {
        $Type = IntegerType::class;
        $this->assertTrue(ATokenType::create($Type) instanceof $Type);
    }

    public function testDetect() {
        $this->assertEquals(IntegerType::class, ATokenType::detect(5));
    }

    public function testNDetect() {
        $type = ATokenType::detect('q');
        $this->assertFalse($type);
    }

}
