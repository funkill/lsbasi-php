<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 12.07.15
 * Time: 14:30
 */

namespace Calculator\Token\Type;

class IntegerTypeTest extends \PHPUnit_Framework_TestCase {

    public function testCheck() {
        $this->assertTrue(IntegerType::check(5));
        $this->assertTrue(IntegerType::check('5'));
    }

    public function testNCheck() {
        $this->assertFalse(IntegerType::check('q'));
    }

    public function testRun() {
        $this->assertFalse(IntegerType::run(1, 1));
    }

}
