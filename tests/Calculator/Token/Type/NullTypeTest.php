<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 12.07.15
 * Time: 14:49
 */

namespace Calculator\Token\Type;

class NullTypeTest extends \PHPUnit_Framework_TestCase {

    public function testCheck() {
        $this->assertTrue(NullType::check(null));
    }

    public function testNCheck() {
        $this->assertFalse(NullType::check('q'));
    }

    public function testRun() {
        $this->assertFalse(NullType::run(1, 1));
    }

}
