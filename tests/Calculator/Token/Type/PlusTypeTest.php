<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 12.07.15
 * Time: 14:51
 */

namespace Calculator\Token\Type;

class PlusTypeTest extends \PHPUnit_Framework_TestCase {

    public function testCheck() {
        $this->assertTrue(PlusType::check('+'));
    }

    public function testNCheck() {
        $this->assertFalse(PlusType::check('q'));
    }

    public function testRun() {
        $this->assertEquals(2, PlusType::run(1, 1));
    }

    public function testNRun() {
        try {
            PlusType::run(1, null);
        } catch (\Exception $E) {
            $this->assertTrue(true);
        }
    }

}
