<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 12.07.15
 * Time: 17:05
 */

namespace Calculator\Token\Type;


class MinusTypeTest extends \PHPUnit_Framework_TestCase {

    public function testCheck() {
        $this->assertTrue(MinusType::check('-'));
    }

    public function testNCheck() {
        $this->assertFalse(MinusType::check('q'));
    }

    public function testRun() {
        $this->assertEquals(0, MinusType::run(1, 1));
    }

    public function testNRun() {
        try {
            MinusType::run(1, null);
        } catch (\Exception $E) {
            $this->assertTrue(true);
        }
    }

}
