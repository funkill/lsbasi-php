<?php

/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 12.07.15
 * Time: 15:06
 */

namespace Calculator\Token\Type;


use Calculator\Exceptions\TypeException;
use Calculator\Token\Token;

class TokenTest extends \PHPUnit_Framework_TestCase {

    public function testInstance() {
        $Token = null;
        try {
            $Token = new Token(5);
        } catch (TypeException $TE) {
            $this->fail('Token not created');
        } catch (\Exception $E) {
            $this->fail('Unknown exception');
        }

        $this->assertTrue($Token instanceof Token);
    }

    public function testNInstance() {
        $Token = null;
        try {
            $Token = new Token('q');
        } catch (TypeException $TE) {
            $this->fail('Token not created');
        } catch (\Exception $E) {
            $this->assertTrue(true);
        }
    }

    public function testStringify() {
        $Token = new Token(5);
        $this->assertTrue(is_string((string)$Token));
    }

}
