<?php
    /**
     * Created by PhpStorm.
     * User: funkill
     * Date: 20.07.15
     * Time: 19:18
     */

    namespace Calculator\Word;


    use Calculator\Exceptions\TypeException;
    use Calculator\Token\Type\ATokenType;

    class WordsFabric {

        /**
         * @param $type
         * @param $value
         * @throws TypeException
         * @return Word
         */
        public static function create($type, $value) {
            if (!in_array($type, ATokenType::types())) {
                throw new TypeException();
            }

            return new Word($type, $value);
        }

    }