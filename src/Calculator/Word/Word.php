<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 12.07.15
 * Time: 14:18
 */

namespace Calculator\Word;


use Calculator\Token\Type\ITokenType;

class Word {
    protected $value;
    protected $type;

    /**
     * @param $type
     * @param $value
     */
    public function __construct($type, $value) {
        $this->type = $type;
        $this->value = $value;
    }

    public function run($a, $b) {
        /**
         * @var $type ITokenType
         */
        $type = $this->type;

        return $type::run($a, $b);
    }

    public function val() {
        return $this->value;
    }
}