<?php

/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 12.07.15
 * Time: 13:32
 */
namespace Calculator\Token;

use Calculator\Exceptions\TypeException;
use Calculator\Token\Type\ATokenType;

class Token {

    protected $type;
    protected $value;

    public function __construct($value) {
        $this->type = ATokenType::detect($value);
        if (!$this->type) {
            throw new TypeException();
        }

        $this->value = $value;
    }

    public function __toString() {
        return sprintf("Token: type `%s`, value `%d`", $this->type, $this->value);
    }

}