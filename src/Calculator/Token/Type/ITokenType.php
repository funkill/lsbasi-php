<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 12.07.15
 * Time: 14:09
 */
namespace Calculator\Token\Type;

interface ITokenType {
    /**
     * @param $type string
     * @return static
     */
    public static function create($type);

    /**
     * @param $value mixed
     * @return bool
     */
    public static function detect($value);

    /**
     * @param $value mixed
     * @return bool
     */
    public static function check($value);

    /**
     * @return array
     */
    public static function types();

    /**
     * @param $a
     * @param $b
     * @return mixed
     */
    public static function run($a, $b);
}