<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 12.07.15
 * Time: 16:53
 */

namespace Calculator\Token\Type;

class MinusType extends ATokenType {

    const VALUE = '-';

    public static function check($value) {
        return static::VALUE == $value;
    }

    public static function run($a, $b) {
        return $a - $b;
    }
}