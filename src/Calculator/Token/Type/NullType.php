<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 12.07.15
 * Time: 13:57
 */

namespace Calculator\Token\Type;

class NullType extends ATokenType {

    const VALUE = null;

    public static function check($value) {
        return static::VALUE === $value;
    }

    public static function run($a, $b) {
        return false;
    }

}