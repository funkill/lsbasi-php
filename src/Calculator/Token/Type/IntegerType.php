<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 12.07.15
 * Time: 13:51
 */

namespace Calculator\Token\Type;

class IntegerType extends ATokenType {

    public static function check($value) {
        if (!is_numeric($value)) {
            return false;
        }

        $value = (int)$value;

        return $value >= 0 && $value <= 9;
    }

    public static function run($a, $b) {
        return false;
    }

}