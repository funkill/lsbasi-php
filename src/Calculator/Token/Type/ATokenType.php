<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 12.07.15
 * Time: 13:37
 */

namespace Calculator\Token\Type;

use Calculator\Exceptions\TypeException;

abstract class ATokenType implements ITokenType {

    const PLUS = PlusType::class;
    const INTEGER = IntegerType::class;
    const NULL = NullType::class;
    const MINUS = MinusType::class;

    /**
     * @param $type
     * @return static
     */
    public static function create($type) {
        return new $type();
    }

    /**
     * @param mixed $value
     * @return string
     * @throws TypeException
     */
    public static function detect($value) {
        /**
         * @var $type static
         */
        foreach (static::types() as $type) {
            if ($type::check($value)) {
                return $type;
            }
        }

        throw new TypeException();
    }

    public static function types() {
        return [
            static::PLUS,
            static::INTEGER,
            static::NULL,
            static::MINUS,
        ];
    }

}
