<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 12.07.15
 * Time: 13:42
 */

namespace Calculator;


use Calculator\Exceptions\TypeException;
use Calculator\Token\Type\ATokenType;
use Calculator\Word\Word;
use Calculator\Word\WordsFabric;

class Interpreter {

    protected $value;
    protected $position;
    protected $tokenType;
    protected $words = [];

    public static function evaluate($string) {
        $result = 0;

        $words = static::wordize($string);

        $wordsCount = count($words);
        for ($i = 0; $i < $wordsCount; ++$i) {
            if (!$result) {
                if (!array_key_exists($i - 1, $words)) {
                    continue;
                }

                /**
                 * @var $curr Word
                 */
                $curr = $words[$i - 1];
                $result = $curr->val();
            }

            if (!array_key_exists($i + 1, $words)) {
                continue;
            }

            /**
             * @var $next Word
             * @var $curr Word
             */
            $next = $words[$i + 1];
            $curr = $words[$i];
            $result = $curr->run($result, $next->val());
        }

        return $result;
    }

    /**
     * @param $evalString string
     * @return array
     * @throws \Exception
     */
    protected static function wordize($evalString) {
        $prevType = ATokenType::NULL;
        $words = [];
        $word = '';
        $tokens = str_split($evalString, 1);

        foreach ($tokens as $token) {
            if (strlen(trim($token)) == 0) { // if it space
                continue;
            }

            $type = ATokenType::detect($token);

            if (!$type) {
                continue;
            }

            if ($prevType != $type) {
                $words[] = WordsFabric::create($prevType, $word);
                $word = '';
                $prevType = $type;
            }

            $word .= $token;
        }

        $words[] = WordsFabric::create($prevType, $word);
        array_shift($words);

        return $words;
    }

}